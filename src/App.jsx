import { useState } from 'react'
import './App.css'
import ComponenteUno from './Components/ActUno/ComponenteUno';
import {ComponenteDos} from './Components/ActUno/ComponenteDos';
import ComponenteTres from './Components/ActUno/ComponenteTres';

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <ComponenteUno />
      <ComponenteDos />
      <ComponenteTres />
    </div>
  )
}

export default App
