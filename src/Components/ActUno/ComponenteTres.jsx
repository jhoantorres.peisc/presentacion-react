import './ComponentUno.css'
import correo from '/src/assets/ActUnoImgs/correo.png'
import facebook from '/src/assets/ActUnoImgs/facebook.png'
import instagram from '/src/assets/ActUnoImgs/instagram.png'
import whatsapp from '/src/assets/ActUnoImgs/whatsapp.png'


function ComponenteTres (){
    return(
        <div className="contactos">
            <div className="icono">
                <a href="https://facebook.com/">
                    <img src={ facebook } alt="" />
                </a>
            </div>
            <div className="icono">
                <a href="https://www.whatsapp.com/">
                    <img src={ whatsapp } alt="" />
                </a>
            </div>
            <div className="icono">
                <a href="https://www.google.com/intl/es-419/gmail/about/">
                    <img src={ correo } alt="" />
                </a>
            </div>
            <div className="icono">
                <a href="https://www.instagram.com/">
                    <img src={ instagram } alt="" />
                </a>
            </div>
        </div>
    )
}

export default ComponenteTres