import './ComponentUno.css'
import perfil from '/src/assets/ActUnoImgs/perfil.webp'

export const ComponenteDos = () => {
    return(
        <>
        <div className="contenedor">
            <div className="imagen">
                <img src={ perfil } alt="" />
            </div>
            <div className="info">
                <h3>Nombre: </h3><a>Alan</a>
                <h3>Edad: </h3><a>21</a>
                <h3>Carrera: </h3><a>ISC</a>
                <h3>Pasatiempo1: </h3><a>Videojuegos</a>
                <h3>Pasatiempo2: </h3><a>Musica</a>
                <h3>Pasatiempo3: </h3><a>Anime</a>
                <h3>Pasatiempo4: </h3><a>Mangas</a>
            </div>
        </div>
        </>

    )
}